DROP TABLE IF EXISTS `message_ctl`;

CREATE TABLE `message_ctl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` varchar(255) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`, `event_id`)
);
