package org.mauro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbControlApp {
    public static void main(String[] args) {
        SpringApplication.run(DbControlApp.class, args);
    }
}