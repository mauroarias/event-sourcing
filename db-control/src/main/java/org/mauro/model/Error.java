package org.mauro.model;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Value
@Builder
@ToString
//Immutable error bean returned when an exception is triggered
public final class Error {
	//Exception's message
	private final String message;
	//Exception type.
	private final String exceptionType;
}
