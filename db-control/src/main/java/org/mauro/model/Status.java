package org.mauro.model;

public enum Status {
	ERROR,
	ACCEPTED,
	WAITING_UPDATE,
	PROCESSED;
}
