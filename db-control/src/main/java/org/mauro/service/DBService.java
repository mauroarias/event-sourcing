package org.mauro.service;

import org.mauro.model.Register;
import org.mauro.model.Status;
import org.mauro.model.exception.NotFoundException;
import org.mauro.repository.RegisterRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

import static java.time.ZoneOffset.UTC;

@Service
public final class DBService {

	private final static Logger logger = LoggerFactory.getLogger(DBService.class);

	private final RegisterRepo registerRepo;

	@Autowired
	public DBService(RegisterRepo registerRepo) {
		this.registerRepo = registerRepo;
	}

	public Register create(final Register register) {
		final ZonedDateTime now = ZonedDateTime.now(UTC);
		register.setCreatedDate(now);
		register.setStatus(Status.ACCEPTED);
		register.setUpdatedDate(now);
		return registerRepo.save(register);
	}

	public Register getByEventId(final String eventId) {
		final Register register = registerRepo.findByEventId(eventId);
		if (register == null) {
			throw new NotFoundException();
		}
		return register;
	}

	public Register UpdateStatusByEventId(final String eventId, final Status status) {
		final Register register = registerRepo.findByEventId(eventId);
		if (register == null) {
			throw new NotFoundException();
		}
		register.setUpdatedDate(ZonedDateTime.now(UTC));
		register.setStatus(status);
		return registerRepo.save(register);
	}
}
