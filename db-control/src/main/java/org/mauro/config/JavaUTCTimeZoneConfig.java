package org.mauro.config;

import org.springframework.context.annotation.Configuration;

import java.util.TimeZone;

@Configuration
public class JavaUTCTimeZoneConfig {

	JavaUTCTimeZoneConfig() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
}
