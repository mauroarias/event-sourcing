package org.mauro.repository;

import org.mauro.model.Register;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface RegisterRepo extends CrudRepository<Register, Long> {

    @Query(value = "SELECT r FROM Register r WHERE r.eventId=:event_id")
    Register findByEventId(@Param("event_id") final String eventId);
}
