package org.mauro.server;

import org.mauro.model.Error;
import org.mauro.model.exception.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public final class HandlerException {

	//unknowns exceptions
	@ExceptionHandler(Exception.class)
	public ResponseEntity handlerException(Exception ex) {
		return new ResponseEntity<>(Error.builder()
										 .exceptionType(ex.getClass().getCanonicalName())
										 .message(ex.getMessage())
										 .build(),
									INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity handlerException(NotFoundException ex) {
		return new ResponseEntity<>(Error.builder()
										 .exceptionType(ex.getClass().getCanonicalName())
										 .message(ex.getMessage())
										 .build(),
									NOT_FOUND);
	}
}
