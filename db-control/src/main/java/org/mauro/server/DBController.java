package org.mauro.server;

import io.micrometer.core.annotation.Timed;
import org.mauro.model.Register;
import org.mauro.model.Status;
import org.mauro.service.DBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "db-control/v1")
@Validated
@Timed
public class DBController {

	private final DBService DBService;

	@Autowired
	DBController(final DBService DBService) {
		this.DBService = DBService;
	}

	@PostMapping(value = "/create", produces = "application/json", consumes = "application/json")
	ResponseEntity create(@RequestBody @Valid @NotNull final Register register) {
		return new ResponseEntity<>(DBService.create(register), CREATED);
	}

	@GetMapping(value = "/event-id/{eventId}", produces = "application/json")
	ResponseEntity getByEventId(@PathVariable @Valid @NotNull final String eventId) {
		return new ResponseEntity<>(DBService.getByEventId(eventId), OK);
	}

	@PatchMapping(value = "/event-id/{eventId}/status/{status}", produces = "application/json")
	ResponseEntity updateStatusByMessageId(@PathVariable @Valid @NotNull final String eventId,
										   @PathVariable @Valid @NotNull final Status status) {
		return new ResponseEntity<>(DBService.UpdateStatusByEventId(eventId, status), OK);
	}
}
