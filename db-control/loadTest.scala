/*
 * Copyright 2011-2018 GatlingCorp (https://gatling.io)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package computerdatabase

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class LoadTest extends Simulation {

//you must increse this value, + 480
  var counter = 3000;

//you must update the session
//  object Register1 {
//    val createRegister = exec(http("poster registration")
//        .get("/template/v1/ping")
//        .header("Content-Type", "application/json")
//        .body(StringBody("""{"accept_new_letters": true, "email": "loadTest01@test.com", "gx_session": "1sOfhBEyaeQFYlq/Y1kLf2gFbX11zeVaiOOJBX5pTiSOmwOucpknn/Nj7q1ywinPIPqYP1WmD8rNqX3jC1kbhsP4tHCzW4lyNGYm5goE/ojM=", "model_name": "SDL9030DB", "platform": "WEB", "product_name": "KARAOKE", "serial_number": "IN-9030DB-18-${numb1}"}"""))
//        .check(status.is(200)))
//  }

////you must update the session
//  object Register2 {
//    val createRegister = exec(http("poster registration")
//        .post("/karaoke-registration/V1/registration")
//        .header("Content-Type", "application/json")
//        .body(StringBody("""{"accept_new_letters": true, "email": "loadTest02@test.com", "gx_session": "1sOfhBEyaeQFYlq/Y1kLf2gFbX11zeVaiOOJBX5pTiSOmwOucpknn/Nj7q1ywinPIPqYP1WmD8rNqX3jC1kbhsP4tHCzW4lyNGYm5goE/ojM=", "model_name": "SDL9030DB", "platform": "WEB", "product_name": "KARAOKE", "serial_number": "IN-9030DB-18-${numb2}"}"""))
//        .check(status.is(201)))
//  }
//
////you must update the session
//  object Register3 {
//    val createRegister = exec(http("poster registration")
//        .post("/karaoke-registration/V1/registration")
//        .header("Content-Type", "application/json")
//        .body(StringBody("""{"accept_new_letters": true, "email": "loadTest03@test.com", "gx_session": "1sOfhBEyaeQFYlq/Y1kLf2gFbX11zeVaiOOJBX5pTiSOmwOucpknn/Nj7q1ywinPIPqYP1WmD8rNqX3jC1kbhsP4tHCzW4lyNGYm5goE/ojM=", "model_name": "SDL9030DB", "platform": "WEB", "product_name": "KARAOKE", "serial_number": "IN-9030DB-18-${numb3}"}"""))
//        .check(status.is(201)))
//  }
//
  val numbers1 = Iterator.continually(Map("numb1" -> getSerial()))
//  val numbers2 = Iterator.continually(Map("numb2" -> getSerial()))
//  val numbers3 = Iterator.continually(Map("numb3" -> getSerial()))

  def getSerial() : String = { counter += 1; counter.toString }

  val httpConf = http
    .baseUrl("http://localhost:8081")

  val scn = scenario("ping")
    .exec(http("ping")
      .get("/template/v1/ping")
      .header("Content-Type", "application/json")
      .check(status.is(200)))

  val scn1 = scenario("ping 1")
    .exec(http("ping")
      .get("/template/v1/ping")
      .header("Content-Type", "application/json")
      .check(status.is(200)))

//  val scn2 = scenario("getter models")
//    .exec(http("get models")
//      .get("/karaoke-registration/V1/registration/models")
//      .header("Content-Type", "application/json")
//      .check(status.is(200)))
//
//  val scn3 = scenario("getter status")
//    .exec(http("get status")
//      .get("/karaoke-registration/V1/registration/mm22@mm.com/status")
//      .header("Content-Type", "application/json")
//      .check(status.is(200)))
//
//  val scn4 = scenario("poster registration 1")
//    .feed(numbers1)
//    .exec(Register1.createRegister)
//
//  val scn5 = scenario("poster registration 2")
//    .feed(numbers2)
//    .exec(Register2.createRegister)
//
//  val scn6 = scenario("poster registration 3")
//    .feed(numbers3)
//    .exec(Register3.createRegister)
    
  setUp(
    scn.inject(rampUsers(5000) during (40 seconds)),
    scn1.inject(rampUsers(5000) during (40 seconds)),
//    scn2.inject(rampUsers(300) during (40 seconds)),
//    scn3.inject(rampUsers(300) during (40 seconds)),
//    scn4.inject(rampUsers(160) during (40 seconds)),
//    scn5.inject(rampUsers(160) during (40 seconds)),
//    scn6.inject(rampUsers(160) during (40 seconds)),
  ).protocols(httpConf)
}
