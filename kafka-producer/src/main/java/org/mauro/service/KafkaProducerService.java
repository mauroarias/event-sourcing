package org.mauro.service;

import org.mauro.client.DbControlClient;
import org.mauro.model.Event;
import org.mauro.model.Register;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public final class KafkaProducerService {

	private final static Logger logger = LoggerFactory.getLogger(KafkaProducerService.class);

	private final KafkaProducer kafkaProducer;
	private final DbControlClient dbControlClient;

	@Autowired
	public KafkaProducerService(final KafkaProducer kafkaProducer,
								final DbControlClient dbControlClient) {
		this.kafkaProducer = kafkaProducer;
		this.dbControlClient = dbControlClient;
	}

	public Event sendEvent(final Event event) {
		kafkaProducer.sendEvent(event);
		dbControlClient.register(event.getEventId(), event.getMessageId());
		return event;
	}

	public Register getStatus(final String eventId) {
		return dbControlClient.getStatus(eventId);
	}
}
