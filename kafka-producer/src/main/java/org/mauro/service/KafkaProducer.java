package org.mauro.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.mauro.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducer {

    private final String topicName;
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper mapper;

    @Autowired
    public KafkaProducer(@Value(value = "${kafka.topic-name}") final String topicName,
                         final KafkaTemplate<String, String> kafkaTemplate,
                         final ObjectMapper mapper) {
        this.topicName = topicName;
        this.kafkaTemplate = kafkaTemplate;
        this.mapper = mapper;
    }

    @SneakyThrows
    public void sendEvent(Event event) {
        kafkaTemplate.send(topicName, mapper.writeValueAsString(event));
    }
}

