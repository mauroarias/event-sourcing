package org.mauro.client;

import org.mauro.model.Register;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DbControlClient {

    private final RestTemplate client;
    private final String BaseRequestPath;

    @Autowired
    public DbControlClient(@Value("${dbcontrol.api.port}") final int port,
                           @Value("${dbcontrol.api.host}") final String host,
                           final RestTemplate client) {
        this.client = client;
        this.BaseRequestPath = "http://" + host + ":" + port + "db-control/v1/";
    }

    public Register register(final String eventId, final String messageId) {
        final HttpEntity<?> request = new HttpEntity<>(Register.builder().eventId(eventId).messageId(messageId).build());
        final ResponseEntity<Register> resp = client.exchange(BaseRequestPath + "create", HttpMethod.POST, request, Register.class);
        if (resp.getStatusCode() != HttpStatus.CREATED) {
            throw new RuntimeException();
        }
        return resp.getBody();
    }

    public Register getStatus(final String eventId) {
        final ResponseEntity<Register> resp = client.getForEntity(BaseRequestPath + "event-id/" + eventId, Register.class);
        if (resp.getStatusCode() != HttpStatus.OK) {
            throw new RuntimeException();
        }
        return resp.getBody();
    }
}
