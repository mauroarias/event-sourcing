package org.mauro.server;

import io.micrometer.core.annotation.Timed;
import org.mauro.model.Event;
import org.mauro.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "kafka-producer/v1")
@Timed
public final class KafkaProducerController {

	private final KafkaProducerService kafkaProducerService;

	@Autowired
    KafkaProducerController(final KafkaProducerService kafkaProducerService) {
		this.kafkaProducerService = kafkaProducerService;
	}

	@PostMapping(value = "/event", produces = "application/json", consumes = "application/json")
	ResponseEntity sendEvent(@RequestBody @Valid @NotNull final Event event) {
		return new ResponseEntity<>(kafkaProducerService.sendEvent(event), ACCEPTED);
	}

	@GetMapping(value = "/event/{eventId}/status", produces = "application/json")
	ResponseEntity getStatus(@PathVariable @Valid @NotNull final String eventId) {
		return new ResponseEntity<>(kafkaProducerService.getStatus(eventId), OK);
	}

}
