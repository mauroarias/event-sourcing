package org.mauro.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.time.ZonedDateTime;

@Value
public class Event {
    private final String eventId;
    private final String messageId;
    private final String field1;
    private final Integer field2;
    private final ZonedDateTime date;

    @JsonCreator
    public Event(final @JsonProperty("event_id") String eventId,
                 final @JsonProperty("message_id") String messageId,
                 final @JsonProperty("field1") String field1,
                 final @JsonProperty("field2") Integer field2,
                 final @JsonProperty("date") ZonedDateTime date) {
        this.eventId = eventId;
        this.messageId = messageId;
        this.field1 = field1;
        this.field2 = field2;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Event [event_id=" + eventId + ", message_id=" + messageId + ", field1=" + field1 + ", field2=" + field2 + ", date=" + date + "]";
    }
}
