package org.mauro.model;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

import java.time.ZonedDateTime;

@Value
@Builder
public class Register {
    private String eventId;
    private String messageId;
    private String status;
    private ZonedDateTime updatedDate;
    private ZonedDateTime createdDate;

    @JsonCreator
    public Register(final @JsonProperty("event_id") String eventId,
                    final @JsonProperty("message_id") String messageId,
                    final @JsonProperty("status") String status,
                    final @JsonProperty("updated_date") ZonedDateTime updatedDate,
                    final @JsonProperty("created_date") ZonedDateTime createdDate) {
        this.eventId = eventId;
        this.messageId = messageId;
        this.status = status;
        this.updatedDate = updatedDate;
        this.createdDate = createdDate;
    }
}
