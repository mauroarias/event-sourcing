package org.mauro.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
public class HttpDbControlRestClientConfig {
    private final RestTemplate client;

    @Autowired
    public HttpDbControlRestClientConfig(@Value("${dbcontrol.api.port}") final int port,
                                         @Value("${dbcontrol.api.host}") final String host,
                                         final ObjectMapper mapper) {
        DefaultUriBuilderFactory uriTemplateFactory =
            new DefaultUriBuilderFactory(String.format("%s%s:%s%s",
                                                       "http://",
                                                       host,
                                                       port,
                                                       "db-control/v1"));
        client = new RestTemplate(new HttpComponentsClientHttpRequestFactory(getHttpClient(port, host)));
        client.setUriTemplateHandler(uriTemplateFactory);

        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setPrettyPrint(false);
        messageConverter.setObjectMapper(mapper);

        client.getMessageConverters().removeIf(m -> m.getClass().getName().equals(MappingJackson2HttpMessageConverter.class.getName()));
        client.getMessageConverters().add(messageConverter);
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return client;
    }

    private CloseableHttpClient getHttpClient(final int port, final String host) {
        // Setting pooling management
        final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(200);
        connectionManager.setMaxPerRoute(new HttpRoute(new HttpHost(host, port, "http")),
                                         200);
        connectionManager.setDefaultMaxPerRoute(200);
        connectionManager.setValidateAfterInactivity(50000);

        // Building httpclient
        final HttpClientBuilder httpClientsBuilder = HttpClientBuilder.create()
            .setDefaultRequestConfig(RequestConfig.DEFAULT)
            .setConnectionManager(connectionManager);
        httpClientsBuilder.disableCookieManagement();
        httpClientsBuilder.useSystemProperties();
        return httpClientsBuilder.build();
    }
}
