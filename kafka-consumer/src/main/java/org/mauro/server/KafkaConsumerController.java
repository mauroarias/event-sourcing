package org.mauro.server;

import io.micrometer.core.annotation.Timed;
import org.mauro.service.KafkaConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "kafka-consumer/v1")
@Timed
public final class KafkaConsumerController {

	private final KafkaConsumerService kafkaConsumerService;

	@Autowired
	KafkaConsumerController(final KafkaConsumerService kafkaConsumerService) {
		this.kafkaConsumerService = kafkaConsumerService;
	}

	@PatchMapping(value = "/event/{eventId}/status/{status}", produces = "application/json")
	ResponseEntity getStatus(@PathVariable @Valid @NotNull final String eventId,
							 @PathVariable @Valid @NotNull final String status) {
		return new ResponseEntity<>(kafkaConsumerService.updateStatus(eventId, status), OK);
	}

}
