package org.mauro.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.PropertyNamingStrategy.SNAKE_CASE;

@Configuration
public class ObjectMapperConfig {

    private final Jackson2ObjectMapperBuilder objectMapperBuilder;

    @Autowired
    public ObjectMapperConfig(final Jackson2ObjectMapperBuilder objectMapperBuilder) {
        this.objectMapperBuilder = objectMapperBuilder;
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper mapper = objectMapperBuilder.createXmlMapper(false).build();
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
		mapper.setPropertyNamingStrategy(SNAKE_CASE);
        mapper.setSerializationInclusion(NON_NULL);

        return mapper;
    }
}
