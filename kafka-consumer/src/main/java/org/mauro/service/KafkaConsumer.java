package org.mauro.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.mauro.client.DbControlClient;
import org.mauro.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    private final static Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    private final DbControlClient dbControlClient;
    private final ObjectMapper mapper;

    @Autowired
    public KafkaConsumer(final DbControlClient dbControlClient, final ObjectMapper mapper) {
        this.dbControlClient = dbControlClient;
        this.mapper = mapper;
    }

    @KafkaListener(topics = "test")
    @SneakyThrows
    public void listen(final String message) {
        final Event event = mapper.readValue(message, Event.class);
        dbControlClient.updateStatus(event.getEventId(), "WAITING_UPDATE");
        logger.debug("message: " + event.toString());
    }
}

