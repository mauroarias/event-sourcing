package org.mauro.service;

import org.mauro.client.DbControlClient;
import org.mauro.model.Register;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public final class KafkaConsumerService {

	private final static Logger logger = LoggerFactory.getLogger(KafkaConsumerService.class);

	private final DbControlClient dbControlClient;

	@Autowired
	public KafkaConsumerService(final DbControlClient dbControlClient) {
		this.dbControlClient = dbControlClient;
	}

	public Register updateStatus(final String eventId, final String status) {
		return dbControlClient.updateStatus(eventId, status);
	}
}
